
const FIRST_NAME = "BORIIA";
const LAST_NAME = "ADRIANA";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
	
	if(value===Infinity || value===-Infinity || value>Number.MAX_SAFE_INTEGER ||value<Number.MIN_SAFE_INTEGER)
		return NaN
	else return parseInt(value) 
    
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

